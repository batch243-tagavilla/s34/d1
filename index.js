// use the "require" directive to load the express module/package

const express = require("express");

// create an application using express function

const app = express();

// for our application server to run, we need a port to listen to
const port = 3000;

// methods used from expressJS are middlewares
// API management is one of the common applications of middlewares

// express.json() allows your app to read json data
app.use(express.json());

// allows your app to read data from forms
app.use(express.urlencoded({ extended: true }));

// [Section] Routes
// create a GET route
// express has methods corresponding to each HTTP method

// this route expects to receive a GET request at the base of the URI "/"
app.get("/", (req, res) => {
    res.send("Hello World");
});

// this route expects to receive a GET request at URI "/hello"
/* app.get("/hello", (req, res) => {
    res.send("Hello from the /hello endpoint")
}); */

// this route expects to receive a POST request at URI "/hello"
// req.body contains the contents/data of the request body
app.use("/hello", (req, res) => {
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
});

// create an array to use as our mock database
// an array that will store our user objects when the "/signup" route is accessed
let users = [];

app.post("/signup", (req, res) => {
    console.log(req.body);
    if (req.body.username !== "" && req.body.password !== "") {
        users.push(req.body);
        res.send(`User ${req.body.username} has successfully registered!`);
    } else {
        res.send(`Please input BOTH username and password.`);
    }
    console.log(users);
});

// this route expects to receive a PUT request at URI "/change-password"
app.put("/change-password", (req, res) => {
    let message;
    for (let i = 0; i < users.length; i++) {
        if (req.body.username == users[i].username) {
            users[i].password = req.body.password;
            message = `User ${req.body.username}'s password has been updated`;
            break;
        } else {
            message = `User "${req.body.username}" does not exists`;
        }
    }
    console.log(users);
    res.send(users);
    res.send(message);
});

/* // if the username and password are not complete, an error message will be sent back to client/postman
if () {

} else {
    res.send(`Please input BOTH username and password`);
} */

app.listen(port, () => {
    console.log(`Server running at port ${port}`);
});
